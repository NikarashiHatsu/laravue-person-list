## Prerequisites
1.  Composer
2.  NPM

## How to install
1.  Copy the `.env.example` and rename it as `.env`.
2.  Open the `.env` file you've copied and change the configuration.
3.  Run `composer install`
4.  Run `php artisan migrate`.
5.  Run `npm install`.
6.  Run `npm run dev`.
7.  Run `php artisan serve` and navigate to the url using your preferred browser.
8.  Use the application.