require('./bootstrap');

// Import Vue
window.Vue = require('vue');
window.$ = window.jQuery = require('jquery');

// Additional Packages
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import Axios from 'axios';

// Use packages as Vue
Vue.use(VueRouter, VueAxios, Axios);

// Import components packages
import App from './components/App';
import Create from './components/Create';
import Read from './components/Read';
import Update from './components/Update';

// Make router
const routes = [
    {
        name: 'read',
        path: '/',
        component: Read,
    },
    {
        name: 'create',
        path: '/create',
        component: Create,
    },
    {
        name: 'update',
        path: '/update/:id',
        component: Update,
    }
];

// Register routes
const router = new VueRouter({ mode: 'history', routes: routes });

// Instantieate vue
new Vue(Vue.util.extend({ router }, App)).$mount("#app");
